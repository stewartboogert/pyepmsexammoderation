
import sys

import pyEPMSExams

if len(sys.argv) < 1:
    print("Please pass driver sheet path")
else:
    driverSheet = sys.argv[1]
    c = pyEPMSExams.ModerationSheetCreator(driverSheet)
