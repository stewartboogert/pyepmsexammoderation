import openpyxl
from openpyxl.chart import BarChart, Reference
from openpyxl.styles import PatternFill
from openpyxl.styles.colors import Color
from openpyxl.utils import get_column_letter
from openpyxl.utils.cell import absolute_coordinate

from .Utils import *
import shutil
import sys
import os


class ModerationSheet :
    COLOR_EDITABLE = Color(rgb="daeef3")
    COLOR_CALCULATED = Color(rgb="ffff99")
    PATTERN_EDITABLE = PatternFill(
        fgColor=COLOR_EDITABLE, patternType="solid"
    )
    PATTERN_CALCULATED = PatternFill(
        fgColor=COLOR_CALCULATED, patternType="solid"
    )

    NOTES_TITLE = "Notes"
    NOTES_WIDTH = 30
    COMPONENT_WIDTH = 10
    SCALED_COMPONENT_WIDTH = 15
    SCALE_BOX_WIDTH = 20
    EX_TOTAL_WIDTH = 18

    BANNER_COORD_EXAM_OUT_OF = (1, 9)
    BANNER_REF_EXAM_OUT_OF = "Banner!{}{}".format(
        get_column_letter(BANNER_COORD_EXAM_OUT_OF[1]),
        BANNER_COORD_EXAM_OUT_OF[0]
    )
    BANNER_Q_FRAC_START = (2, 5)

    ##################################################################################################
    def __init__(self, name) :
        self.name = name
        self.output_wb = openpyxl.Workbook()

    ##################################################################################################
    def addInstructionSheet(self, name = "Instructions") :
        ws = self.output_wb.create_sheet(name)

        ws.cell(1, 1).value = "Order"
        ws.cell(2, 1).value = "1"
        ws.cell(3, 1).value = "2"
        ws.cell(4, 1).value = "3"
        ws.cell(5, 1).value = "4"
        ws.cell(6, 1).value = "5"
        ws.cell(7, 1).value = "6"
        ws.cell(8, 1).value = "7"
        ws.cell(9, 1).value = "8"
        ws.cell(10,1).value = "9"
        ws.cell(11,1).value = "10"
        ws.cell(12,1).value = "11"
        ws.cell(13,1).value = "12"


        ws.cell(1, 2).value = "Instruction"
        ws.cell(2, 2).value = "Download from SharePoint on to your own machine"
        ws.cell(3, 2).value = "Review Component set up in Banner tab"
        ws.cell(4, 2).value = "If your exam is NOT out of 100 you can change the 'out of' number in the  Exam total marks cell on the Banner tab"
        ws.cell(5, 2).value = "Order by candidate numbers on each tab - smallest to largest"
        ws.cell(6, 2).value = "Enter exam marks on exam tab ( EX/EXAM)"
        ws.cell(7, 2).value = "If CW marks incomplete contact the admin team"
        ws.cell(8, 2).value = "View moderation tab "
        ws.cell(9, 2).value = "Complete moderation"
        ws.cell(10, 2).value = "Discuss potential adjustment with DAB chair"
        ws.cell(11, 2).value = "Sign off"
        ws.cell(12, 2).value = "Complete any tracking required by the Department DAB chair/admin team"
        ws.cell(13, 2).value = "Rename as FINAL and save back to SharePoint, Email EPMS-school@rhul.ac.uk to let the admin team know its ready"

        ws.cell(15, 2).value = "If you are unsure on how to use the moderation sheets you can ask your DAB chair or the admin team"

        ws.cell(1, 3).value = "Notes"
        ws.cell(2, 3).value = "Not all functions of excel work on SharePoint (particuarly scaling)"
        ws.cell(3, 3).value = "All components should be listed, if not contact the admin team"
        ws.cell(4, 3).value = "This feature will change the formula on the exam mark tab for you, any problem contact the admin team"
        ws.cell(5, 3).value = "Column A select drop down menu (on EX/EXAM sheet).\nIf adding extra notes or additional columns remove and re add the filters so all data is resorted correctly.\nIf a candidate number is not listed contact the admin team."
        ws.cell(6, 3).value = "For additional questions add more columns as necessary, no need to delete unused columns"
        ws.cell(7, 3).value = ""
        ws.cell(8, 3).value = "Shows total average and numbers in each class"
        ws.cell(9, 3).value = "Consider moderation in line with school norms, data from previous years is on SharePoint alongside moderation sheet "
        ws.cell(10, 3).value = ""
        ws.cell(11,3).value = "Sign off each stage (marking, 2nd marking, EE review)"
        ws.cell(12,3).value = ""
        ws.cell(13,3).value = ""

        ws.column_dimensions['A'].width = 10
        ws.column_dimensions['B'].width = 100
        ws.column_dimensions['C'].width = 100

        ws.row_dimensions[5].height = 40

    ##################################################################################################
    def addSubComponentSheet(self, name, sheet, weight = 0) :
        ws = self.output_wb.create_sheet(name)
        WorksheetCopy(sheet,ws)
        self.colorColumns(ws, [6])
        self.addNotes(ws, ws.max_column + 1)
        ws.auto_filter.ref=("A1:{end}".format(end = ws.cell(ws.max_row,ws.max_column).coordinate))

    ##################################################################################################
    def addComponentSheet(self, name, sheet, weight = 0) :
        ws = self.output_wb.create_sheet(name)
        WorksheetCopy(sheet,ws)
        self.colorColumns(ws, [6])
        self.addNotes(ws, ws.max_column + 1)
        ws.auto_filter.ref=("A1:{end}".format(end = ws.cell(ws.max_row,ws.max_column).coordinate))

    ##################################################################################################
    def addExamSheet(self, sheet, nQuestions = 0, questionNames = [], questionWeight = []) :

        ws = self.output_wb.create_sheet("EX")
        WorksheetCopy(sheet,ws)

        iExamOffset = ws.max_column + 2
        iExamFinal  = 6

        # number format of exam final col
        for j in range(2,ws.max_row+1) :
            ws.cell(j,iExamFinal).number_format = '0'

        # detect if pre-existing exams marks are available
        pre_existing_exams_marks = False
        for j in range(2,ws.max_row+1):
            if ws.cell(j,iExamFinal).value is not None:
                pre_existing_exams_marks = True
                break

        if pre_existing_exams_marks:
            self.colorColumns(ws, [iExamFinal])
        else:
            # make exam questions headings
            for i in range(0,nQuestions) :
                ws.cell(1,iExamOffset+i).value = questionNames[i]

            iRawTotalCol = iExamOffset + nQuestions
            iExamOutOfCol = iExamOffset + nQuestions + 1
            iNormedTotalCol = iExamOffset + nQuestions + 2

            # make exam total columns
            ws.cell(1,iRawTotalCol).value   = "Raw total marks"
            ws.cell(1,iExamOutOfCol).value = "Exam out of"
            ws.cell(1,iNormedTotalCol).value = "Total out of 100"
            self.setColWidth(ws, iRawTotalCol, self.EX_TOTAL_WIDTH)
            self.setColWidth(ws, iExamOutOfCol, self.EX_TOTAL_WIDTH)
            self.setColWidth(ws, iNormedTotalCol, self.EX_TOTAL_WIDTH)

            # loop over candidates
            for j in range(2,ws.max_row+1) :
                ws.cell(j,iExamFinal).number_format = '0'

                ws.cell(j,iRawTotalCol).value \
                    = "=SUM({cell1}:{cell2})".format(
                        cell1 = ws.cell(j,iExamOffset).coordinate,
                        cell2 = ws.cell(j,iExamOffset+nQuestions-1).coordinate
                    )
                ws.cell(j,iRawTotalCol).number_format = '0'

                ws.cell(j,iExamOutOfCol).value \
                    = f"={self.BANNER_REF_EXAM_OUT_OF}"

                ws.cell(j,iNormedTotalCol).value \
                    = "=ROUND({cell}/{total}*100,0)".format(
                        cell=ws.cell(j,iRawTotalCol).coordinate,
                        total=ws.cell(j,iExamOutOfCol).coordinate
                    )
                ws.cell(j,iNormedTotalCol).number_format = '0'

                ws.cell(j,iExamFinal).value = "={cell}".format(
                    cell= ws.cell(j,iNormedTotalCol).coordinate
                )

            iNotesCol = iNormedTotalCol + 1
            self.addNotes(ws, iNotesCol)

            self.colorColumns(ws,
                editable_cols=
                    [iExamOffset + col for col in range(nQuestions)]
                    + [iNotesCol],
                calculated_cols=[
                    iExamFinal, iRawTotalCol, iExamOutOfCol, iNormedTotalCol
                ]
            )

        # add sorting
        ws.auto_filter.ref=("A1:{end}".format(end = ws.cell(ws.max_row,ws.max_column).coordinate))

    ##################################################################################################
    def addModerationSheet(self, sheet, iComponentOffset = 5, componentName = [], componentSubName = [], componentWeight = []) :

        def makeTotalFormula(componentsOffset):
            totalFormula = "=ROUND("
            for i, _, cs in zip(range(0, nComponent), componentName, componentSubName):
                if cs is None :
                    weight = componentWeight[i]
                    cscell = ws.cell(j,componentsOffset + i).coordinate
                    if i > 0:
                        totalFormula += " + "
                    totalFormula += f"({cscell}*{weight})"
            totalFormula += ", 0)"
            return totalFormula

        ws = self.output_wb.create_sheet("Moderation")
        WorksheetCopy(sheet,ws)

        nComponent = len(componentName)

        # make component headings
        for i,c,cs in zip(range(0,nComponent), componentName, componentSubName) :
            if cs is None :
                ws.cell(1,iComponentOffset+i).value = c
            else :
                ws.cell(1,iComponentOffset+i).value = c+" "+cs

            # style
            ws.cell(1,iComponentOffset + i).fill = copy(ws.cell(1, 1).fill)
            ws.cell(1,iComponentOffset + i).font = copy(ws.cell(1, 1).font)

            self.setColWidth(ws, iComponentOffset + i, self.COMPONENT_WIDTH)


        # fill source data from sub component sheets
        for j in range(2,ws.max_row+1) :

            for i,c,cs in zip(range(0,nComponent), componentName, componentSubName) :

                # content of cell
                if cs is None :
                    sourceWsName = c
                    cCandidateNumber = ws.cell(j, 1).coordinate
                    ws.cell(j,iComponentOffset+i).value = "=VLOOKUP({candnum},{com}!A2:F{maxcand},6, FALSE)".format(candnum = cCandidateNumber,
                                                                                                                    com     = sourceWsName,
                                                                                                                    maxcand = ws.max_row)
                    ws.cell(j, iComponentOffset + i).number_format = '0'
                else :
                    sourceWsName = c+" "+cs
                    cCandidateNumber = ws.cell(j, 1).coordinate
                    ws.cell(j, iComponentOffset + i).value = "=VLOOKUP({candnum},'{com}'!A2:I{maxcand},9, FALSE)".format(candnum=cCandidateNumber,
                                                                                                                            com=sourceWsName,
                                                                                                                            maxcand=ws.max_row)
                    ws.cell(j, iComponentOffset + i).number_format = '0'

                # style
                ws.cell(j, iComponentOffset+i).fill = copy(ws.cell(j, 1).fill)
                ws.cell(j, iComponentOffset+i).font = copy(ws.cell(j, 1).font)

            totalFormula = makeTotalFormula(iComponentOffset)
            ws.cell(j,iComponentOffset+nComponent).value = totalFormula
            ws.cell(j,iComponentOffset+nComponent).number_format = '0'

        ncom = 0
        for cs in componentSubName :
            if cs is None :
                ncom += 1

        # blank gap column
        for j in range(1, ws.max_row+1):
            ws.cell(j, iComponentOffset + nComponent + 1).value = ""
        self.setColWidth(
            ws,
            iComponentOffset + nComponent + 1,
            self.COMPONENT_WIDTH
        )

        # fill in scaling data
        scalingTableRanges = dict()
        numTables = 0
        for c, cs in zip(componentName, componentSubName):
            if cs is None:
                tblScaleCol = \
                    iComponentOffset + nComponent + ncom + 4 + 2 * numTables
                scalingTableRanges[c] = \
                    self._insertScalingTable(ws, c, 1, tblScaleCol)
                numTables += 1

        iScaledComponentOffset = iComponentOffset + nComponent + 2
        icom = 0
        for j in range(2,ws.max_row+1) :
            icom = 0

            for i, c, cs in zip(range(0, nComponent), componentName, componentSubName):
                if j ==2 :
                    ws.cell(1, iScaledComponentOffset + icom).value = c+" (scaled)"
                    ws.cell(1, iScaledComponentOffset + icom).fill  = copy(ws.cell(1,1).fill)
                    ws.cell(1, iScaledComponentOffset + icom).font  = copy(ws.cell(1,1).font)
                    self.setColWidth(
                        ws,
                        iScaledComponentOffset + icom,
                        self.SCALED_COMPONENT_WIDTH
                    )

                if cs is None :
                    cscell = ws.cell(j, iScaledComponentOffset + icom)

                    origMark = ws.cell(j, iComponentOffset + i).coordinate

                    scaledRange, origRange = scalingTableRanges[c]

                    cscell.value = self._getScalingFormula(
                        origMark, scaledRange, origRange
                    )

                    cscell.number_format = '0'
                    # style
                    cscell.fill = copy(ws.cell(j, 1).fill)
                    cscell.font = copy(ws.cell(j, 1).font)
                    cscell.number_format = '0'

                    icom += 1


            totalFormula = makeTotalFormula(iComponentOffset + nComponent + 2)

            totalCell = ws.cell(j,iComponentOffset + nComponent + icom + 2)
            totalCell.value = totalFormula
            totalCell.number_format = '0'

        self.addNotes(ws, iComponentOffset + nComponent + icom + 3)
        self.colorColumns(ws,
            calculated_cols=range(3, iComponentOffset + nComponent + icom + 3)
        )

        # make module total column title
        ws.cell(1,iComponentOffset+nComponent).value = "Total"
        ws.cell(1,iComponentOffset+nComponent).fill  = copy(ws.cell(1, 1).fill)
        ws.cell(1,iComponentOffset+nComponent).font  = copy(ws.cell(1, 1).font)
        self.setColWidth(ws, iComponentOffset+nComponent, self.COMPONENT_WIDTH)

        # make module total column title
        ws.cell(1,iComponentOffset+nComponent+icom+2).value = "Total (scaled)"
        ws.cell(1,iComponentOffset+nComponent+icom+2).fill  = copy(ws.cell(1, 1).fill)
        ws.cell(1,iComponentOffset+nComponent+icom+2).font  = copy(ws.cell(1, 1).font)
        self.setColWidth(
            ws, iComponentOffset+nComponent+icom+2, self.SCALED_COMPONENT_WIDTH
        )

        unscaledCl = ws.cell(1,iComponentOffset+nComponent).column_letter
        scaledCl   = ws.cell(1,iComponentOffset+nComponent+icom+2).column_letter

        # add summary statistics

        self.setColWidth(
            ws,
            iComponentOffset + nComponent + icom + 6,
            self.SCALE_BOX_WIDTH
        )

        summaryGap = 5

        ws.cell(summaryGap + 9, iComponentOffset + nComponent + icom + 5).value = "Unscaled"
        ws.cell(summaryGap + 9, iComponentOffset + nComponent + icom + 6).value = "Scaled"

        ws.cell(summaryGap + 10, iComponentOffset + nComponent + icom + 4).value = "0 - 9"
        ws.cell(summaryGap + 11, iComponentOffset + nComponent + icom + 4).value = "10 - 19"
        ws.cell(summaryGap + 12, iComponentOffset + nComponent + icom + 4).value = "20 - 29"
        ws.cell(summaryGap + 13, iComponentOffset + nComponent + icom + 4).value = "30 - 39"
        ws.cell(summaryGap + 14, iComponentOffset + nComponent + icom + 4).value = "40 - 49"
        ws.cell(summaryGap + 15, iComponentOffset + nComponent + icom + 4).value = "50 - 59"
        ws.cell(summaryGap + 16, iComponentOffset + nComponent + icom + 4).value = "60 - 69"
        ws.cell(summaryGap + 17, iComponentOffset + nComponent + icom + 4).value = "70 - 79"
        ws.cell(summaryGap + 18, iComponentOffset + nComponent + icom + 4).value = "80 - 89"
        ws.cell(summaryGap + 19, iComponentOffset + nComponent + icom + 4).value = "90 - 100"

        ws.cell(summaryGap + 10, iComponentOffset + nComponent + icom + 5).value = '=COUNTIF({cl}:{cl},"<10")'.format(cl=unscaledCl)
        ws.cell(summaryGap + 11, iComponentOffset + nComponent + icom + 5).value = '=COUNTIF({cl}:{cl},"<20") - COUNTIF({cl}:{cl},"<10")'.format(cl=unscaledCl)
        ws.cell(summaryGap + 12, iComponentOffset + nComponent + icom + 5).value = '=COUNTIF({cl}:{cl},"<30") - COUNTIF({cl}:{cl},"<20")'.format(cl=unscaledCl)
        ws.cell(summaryGap + 13, iComponentOffset + nComponent + icom + 5).value = '=COUNTIF({cl}:{cl},"<40") - COUNTIF({cl}:{cl},"<30")'.format(cl=unscaledCl)
        ws.cell(summaryGap + 14, iComponentOffset + nComponent + icom + 5).value = '=COUNTIF({cl}:{cl},"<50") - COUNTIF({cl}:{cl},"<40")'.format(cl=unscaledCl)
        ws.cell(summaryGap + 15, iComponentOffset + nComponent + icom + 5).value = '=COUNTIF({cl}:{cl},"<60") - COUNTIF({cl}:{cl},"<50")'.format(cl=unscaledCl)
        ws.cell(summaryGap + 16, iComponentOffset + nComponent + icom + 5).value = '=COUNTIF({cl}:{cl},"<70") - COUNTIF({cl}:{cl},"<60")'.format(cl=unscaledCl)
        ws.cell(summaryGap + 17, iComponentOffset + nComponent + icom + 5).value = '=COUNTIF({cl}:{cl},"<80") - COUNTIF({cl}:{cl},"<70")'.format(cl=unscaledCl)
        ws.cell(summaryGap + 18, iComponentOffset + nComponent + icom + 5).value = '=COUNTIF({cl}:{cl},"<90") - COUNTIF({cl}:{cl},"<80")'.format(cl=unscaledCl)
        ws.cell(summaryGap + 19, iComponentOffset + nComponent + icom + 5).value = '=COUNTIF({cl}:{cl},"<=100") - COUNTIF({cl}:{cl},"<90")'.format(cl=unscaledCl)

        ws.cell(summaryGap + 10, iComponentOffset + nComponent + icom + 6).value = '=COUNTIF({cl}:{cl},"<10")'.format(cl=scaledCl)
        ws.cell(summaryGap + 11, iComponentOffset + nComponent + icom + 6).value = '=COUNTIF({cl}:{cl},"<20") - COUNTIF({cl}:{cl},"<10")'.format(cl=scaledCl)
        ws.cell(summaryGap + 12, iComponentOffset + nComponent + icom + 6).value = '=COUNTIF({cl}:{cl},"<30") - COUNTIF({cl}:{cl},"<20")'.format(cl=scaledCl)
        ws.cell(summaryGap + 13, iComponentOffset + nComponent + icom + 6).value = '=COUNTIF({cl}:{cl},"<40") - COUNTIF({cl}:{cl},"<30")'.format(cl=scaledCl)
        ws.cell(summaryGap + 14, iComponentOffset + nComponent + icom + 6).value = '=COUNTIF({cl}:{cl},"<50") - COUNTIF({cl}:{cl},"<40")'.format(cl=scaledCl)
        ws.cell(summaryGap + 15, iComponentOffset + nComponent + icom + 6).value = '=COUNTIF({cl}:{cl},"<60") - COUNTIF({cl}:{cl},"<50")'.format(cl=scaledCl)
        ws.cell(summaryGap + 16, iComponentOffset + nComponent + icom + 6).value = '=COUNTIF({cl}:{cl},"<70") - COUNTIF({cl}:{cl},"<60")'.format(cl=scaledCl)
        ws.cell(summaryGap + 17, iComponentOffset + nComponent + icom + 6).value = '=COUNTIF({cl}:{cl},"<80") - COUNTIF({cl}:{cl},"<70")'.format(cl=scaledCl)
        ws.cell(summaryGap + 18, iComponentOffset + nComponent + icom + 6).value = '=COUNTIF({cl}:{cl},"<90") - COUNTIF({cl}:{cl},"<80")'.format(cl=scaledCl)
        ws.cell(summaryGap + 19, iComponentOffset + nComponent + icom + 6).value = '=COUNTIF({cl}:{cl},"<=100") - COUNTIF({cl}:{cl},"<90")'.format(cl=scaledCl)

        ws.cell(summaryGap + 20, iComponentOffset + nComponent + icom + 4).value = "Entries"
        ws.cell(summaryGap + 21, iComponentOffset + nComponent + icom + 4).value = "Mean"
        ws.cell(summaryGap + 22, iComponentOffset + nComponent + icom + 4).value = "I"
        ws.cell(summaryGap + 23, iComponentOffset + nComponent + icom + 4).value = "IIa"
        ws.cell(summaryGap + 24, iComponentOffset + nComponent + icom + 4).value = "IIb"
        ws.cell(summaryGap + 25, iComponentOffset + nComponent + icom + 4).value = "III"
        ws.cell(summaryGap + 27, iComponentOffset + nComponent + icom + 4).value = "F"

        ws.cell(summaryGap + 20, iComponentOffset + nComponent + icom + 5).value = '=COUNT(A2:A1000)'
        ws.cell(summaryGap + 21, iComponentOffset + nComponent + icom + 5).value = '=AVERAGE({cl}2:{cl}1000)'.format(cl=unscaledCl)
        ws.cell(summaryGap + 21, iComponentOffset + nComponent + icom + 5).number_format = "0.0"
        ws.cell(summaryGap + 22, iComponentOffset + nComponent + icom + 5).value = '=COUNTIF({cl}:{cl},">=70")'.format(cl=unscaledCl)
        ws.cell(summaryGap + 23, iComponentOffset + nComponent + icom + 5).value = '=COUNTIF({cl}:{cl},"<70") - COUNTIF({cl}:{cl},"<60")'.format(cl=unscaledCl)
        ws.cell(summaryGap + 24, iComponentOffset + nComponent + icom + 5).value = '=COUNTIF({cl}:{cl},"<60") - COUNTIF({cl}:{cl},"<50")'.format(cl=unscaledCl)
        ws.cell(summaryGap + 25, iComponentOffset + nComponent + icom + 5).value = '=COUNTIF({cl}:{cl},"<50") - COUNTIF({cl}:{cl},"<40")'.format(cl=unscaledCl)
        ws.cell(summaryGap + 27, iComponentOffset + nComponent + icom + 5).value = '=COUNTIF({cl}:{cl},"<40")'.format(cl=unscaledCl)

        ws.cell(summaryGap + 20, iComponentOffset + nComponent + icom + 6).value = '=COUNT(A2:A1000)'
        ws.cell(summaryGap + 21, iComponentOffset + nComponent + icom + 6).value = '=AVERAGE({cl}2:{cl}1000)'.format(cl =scaledCl)
        ws.cell(summaryGap + 21, iComponentOffset + nComponent + icom + 6).number_format = "0.0"
        ws.cell(summaryGap + 22, iComponentOffset + nComponent + icom + 6).value = '=COUNTIF({cl}:{cl},">=70")'.format(cl=scaledCl)
        ws.cell(summaryGap + 23, iComponentOffset + nComponent + icom + 6).value = '=COUNTIF({cl}:{cl},"<70") - COUNTIF({cl}:{cl},"<60")'.format(cl=scaledCl)
        ws.cell(summaryGap + 24, iComponentOffset + nComponent + icom + 6).value = '=COUNTIF({cl}:{cl},"<60") - COUNTIF({cl}:{cl},"<50")'.format(cl=scaledCl)
        ws.cell(summaryGap + 25, iComponentOffset + nComponent + icom + 6).value = '=COUNTIF({cl}:{cl},"<50") - COUNTIF({cl}:{cl},"<40")'.format(cl=scaledCl)
        ws.cell(summaryGap + 27, iComponentOffset + nComponent + icom + 6).value = '=COUNTIF({cl}:{cl},"<40")'.format(cl=scaledCl)


        # add histograms
        unscaledChart = BarChart()
        unscaledChart.type = "col"
        unscaledChart.style = 10
        unscaledChart.title = "Raw results"
        unscaledChart.y_axis.title = '# Students'
        unscaledChart.x_axis.title = 'Raw grade'

        unscaledChartCats = Reference(ws,
                                      min_col=iComponentOffset + nComponent + icom + 4,
                                      max_col=iComponentOffset + nComponent + icom + 4,
                                      min_row=summaryGap + 10,
                                      max_row=summaryGap + 19)
        unscaledChartData = Reference(ws,
                                      min_col=iComponentOffset + nComponent + icom + 5,
                                      max_col=iComponentOffset + nComponent + icom + 5,
                                      min_row=summaryGap + 10,
                                      max_row=summaryGap + 19)

        unscaledChart.add_data(unscaledChartData)
        unscaledChart.set_categories(unscaledChartCats)

        ws.add_chart(unscaledChart, ws.cell(10,iComponentOffset+nComponent+icom+10).coordinate)


        scaledChart = BarChart()
        scaledChart.type = "col"
        scaledChart.style = 10
        scaledChart.title = "Scaled results"
        scaledChart.y_axis.title = '# Students'
        scaledChart.x_axis.title = 'Scaled grade'

        scaledChartCats = Reference(ws,
                                      min_col=iComponentOffset + nComponent + icom + 4,
                                      max_col=iComponentOffset + nComponent + icom + 4,
                                      min_row=summaryGap + 10,
                                      max_row=summaryGap + 19)
        scaledChartData = Reference(ws,
                                      min_col=iComponentOffset + nComponent + icom + 6,
                                      max_col=iComponentOffset + nComponent + icom + 6,
                                      min_row=summaryGap + 10,
                                      max_row=summaryGap + 19)

        scaledChart.add_data(scaledChartData)
        scaledChart.set_categories(scaledChartCats)

        ws.add_chart(scaledChart, ws.cell(25,iComponentOffset+nComponent+icom+10).coordinate)

        # add sorting
        ws.auto_filter.ref=("A1:{end}".format(end = ws.cell(ws.max_row,iComponentOffset+nComponent+icom+3).coordinate))
        # ws.auto_filter.add_sort_condition("A2:A1000")

        return iScaledComponentOffset

    def _getScalingFormula(self, origMarkCell, scaledRange, origRange):
        """Get scaling formula for exam

        Below uses a table of e.g.
        Exam Scaled | Exam Original
           0        |      0
          40        |     30
          50        |     40
        ....
        i.e. original score of 30 should scale to 40

        Table assumed to start with headers on Row 1, but col may vary.

        :param origMarkCell: string of cell coord where mark is, e.g. A2
        :param colScaled: string of the range of exam scaled column
        (e.g. B2:B9)
        :param colOrig: string of the range of exam orig colum"""

        # which row of the scale table the mark lies in
        sclIndex = f"MATCH({origMarkCell},{origRange},1)"
        # boundaries of scaling
        sclBase = f"INDEX({scaledRange}, {sclIndex})"
        sclNextBase = f"INDEX({scaledRange}, {sclIndex} + 1)"
        origBase = f"INDEX({origRange}, {sclIndex})"
        origNextBase = f"INDEX({origRange}, {sclIndex} + 1)"
        # Size of current index (e.g. from 40 to 50 is 10)
        sclSize = f"({sclNextBase} - {sclBase})"
        origSize = f"({origNextBase} - {origBase})"
        origOffset = f"({origMarkCell} - {origBase})"

        scalingFmla = \
            f"{sclBase} + {origOffset} * {sclSize} / {origSize}"

        return f"=ROUND({scalingFmla}, 0)"

    def _insertScalingTable(self, ws, name, tblRow, tblCol):
        """Add a table for scaling

        See _getScalingFormula for format.
        :param ws: worksheet to add table to
        :param name: the name of the component to be scaled (for heading)
        :param tblRow: which row to start the table at
        :param tblCol: which col to start the table at
        :returns: scaledRange, origRange string representing ranges of
        the scaled and original columns."""

        ws.cell(tblRow, tblCol).value = f"{name} grade boundary"
        ws.cell(tblRow, tblCol + 1).value = f"{name} mark to be scaled"
        self.setColWidth(ws, tblCol, self.SCALE_BOX_WIDTH)
        self.setColWidth(ws, tblCol + 1, self.SCALE_BOX_WIDTH)

        ws.cell(tblRow + 1, tblCol).value = 0
        ws.cell(tblRow + 2, tblCol).value = 30
        ws.cell(tblRow + 3, tblCol).value = 40
        ws.cell(tblRow + 4, tblCol).value = 50
        ws.cell(tblRow + 5, tblCol).value = 60
        ws.cell(tblRow + 6, tblCol).value = 70
        ws.cell(tblRow + 7, tblCol).value = 100.000001
        ws.cell(tblRow + 7, tblCol).number_format = '0'

        ws.cell(tblRow + 1, tblCol + 1).value = 0
        ws.cell(tblRow + 2, tblCol + 1).value = 30
        ws.cell(tblRow + 3, tblCol + 1).value = 40
        ws.cell(tblRow + 4, tblCol + 1).value = 50
        ws.cell(tblRow + 5, tblCol + 1).value = 60
        ws.cell(tblRow + 6, tblCol + 1).value = 70
        ws.cell(tblRow + 7, tblCol + 1).value = 100.000001
        ws.cell(tblRow + 7, tblCol + 1).number_format = '0'

        self.colorColumns(ws,
            editable_cells=[ (4, tblCol + 1), (7, tblCol + 1) ]
        )

        scaledRange = absolute_coordinate(
            ws.cell(tblRow + 1, tblCol).coordinate
        ) + ":" + absolute_coordinate(
            ws.cell(tblRow + 7, tblCol).coordinate
        )

        origRange = absolute_coordinate(
            ws.cell(tblRow + 1, tblCol + 1).coordinate
        ) + ":" + absolute_coordinate(
            ws.cell(tblRow + 7, tblCol + 1).coordinate
        )

        return scaledRange, origRange


    ##################################################################################################
    def addFinalMarkSheet(self, sheet, iModerationColumn):
        ws = self.output_wb.create_sheet("Final Grade")
        WorksheetCopy(sheet,ws)

        iFinalOffset = 5

        lModerationColumn = ws.cell(1,iModerationColumn).column_letter

        for j in range(2,ws.max_row+1) :
            cCandidateNumber = ws.cell(j,1).coordinate
            ws.cell(j,iFinalOffset).value = "=VLOOKUP({cn}, {range}, {coln}, FALSE)".format(cn     = cCandidateNumber,
                                                                                            range  = "Moderation!A2:"+lModerationColumn+str(ws.max_row),
                                                                                            coln   = iModerationColumn)
        ws.auto_filter.ref = ("A1:{end}".format(end=ws.cell(ws.max_row, ws.max_column).coordinate))

    def addSignOffSheet(self) :
        ws = self.output_wb.create_sheet("Sign off")

        ws.cell(1,1).value = "Date"
        ws.cell(1,2).value = "Person(s) (full name)"
        ws.cell(1,3).value = "Role"
        ws.cell(1,4).value = "Action/Note"

        ws.column_dimensions['A'].width = 10
        ws.column_dimensions['B'].width = 50
        ws.column_dimensions['C'].width = 75
        ws.column_dimensions['D'].width = 75

    def addScaledSheet(self, sheet, name, iScaledColumn):
        ws = self.output_wb.create_sheet(f"{name} Mark for Upload (scaled)")
        WorksheetCopy(sheet,ws)

        iFinalOffset = 6

        lScaledColumn = get_column_letter(iScaledColumn)

        for j in range(2,ws.max_row+1) :
            cCandidateNumber = ws.cell(j,1).coordinate
            ws.cell(j,iFinalOffset).value \
                = "=VLOOKUP({cn}, {range}, {coln}, FALSE)".format(
                    cn = cCandidateNumber,
                    range = "Moderation!A2:"+lScaledColumn+str(ws.max_row),
                    coln = iScaledColumn,
                )

        iNotesCol = ws.max_column + 1

        self.addNotes(ws, iNotesCol)

        ws.auto_filter.ref = ("A1:{end}".format(
            end=ws.cell(ws.max_row, ws.max_column).coordinate)
        )

        self.colorColumns(ws,
            editable_cols=[iNotesCol],
            calculated_cols=[iFinalOffset]
        )

    def addDriver(self,sheet) :
        ws = self.output_wb.create_sheet("Banner")
        WorksheetCopy(sheet,ws)

        # Remove Question/Fraction table
        for row in range(self.BANNER_Q_FRAC_START[0], ws.max_row + 1):
            for coloff in range(2):
                col = self.BANNER_Q_FRAC_START[1] + coloff
                ws.cell(row, col).value = ""

        self.colorColumns(
            ws, editable_cells=[
                self.BANNER_COORD_EXAM_OUT_OF
            ]
        )

    ##################################################################################################
    def saveWorkbook(self, filePath):
        self.output_wb.remove_sheet(self.output_wb.get_sheet_by_name("Sheet"))
        self.output_wb.save(filename = filePath)

    ##################################################################################################

    def colorColumns(
        self,
        sheet,
        editable_cols = [], calculated_cols = [],
        editable_cells = [], calculated_cells = []
    ):
        for col in editable_cols:
            for row in range(1,sheet.max_row+1) :
                sheet.cell(row, col).fill = self.PATTERN_EDITABLE

        for col in calculated_cols:
            for row in range(1,sheet.max_row+1) :
                sheet.cell(row, col).fill = self.PATTERN_CALCULATED

        for row, col in editable_cells:
            sheet.cell(row, col).fill = self.PATTERN_EDITABLE

        for row, col in calculated_cells:
            sheet.cell(row, col).fill = self.PATTERN_CALCULATED

    def addNotes(self, sheet, iNotesCol):
        sheet.cell(1,iNotesCol).value = self.NOTES_TITLE
        self.setColWidth(sheet, iNotesCol, self.NOTES_WIDTH)
        self.colorColumns(sheet, [iNotesCol])

    def setColWidth(self, sheet, iCol, iWidth):
        sheet.column_dimensions[get_column_letter(iCol)].width = iWidth

