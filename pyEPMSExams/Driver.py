import openpyxl
from pathlib import Path as _Path
import os

from .ModerationSheet import *

class ModerationSheetCreator :
    ##################################################################################################
    def __init__(self, driverSheetPath) :

        #
        self.iRowStart        = 3
        self.iComponentColumn = 1
        self.iExamColumn      = 5

        #
        self.componentName      = []
        self.componentSubName   = []
        self.componentWeight    = []

        self.questionName     = []
        self.questionWeight   = []

        # path to driver sheet
        self.driverSheetPath     = _Path(driverSheetPath)
        self.driverSheetDir      = self.driverSheetPath.parents[0]
        self.driverSheetFileName = self.driverSheetPath.parts[-1]
        self.driverSheetFileStub = self.driverSheetFileName[0:self.driverSheetFileName.rfind(".")]

        # open driver sheet
        self.driver_wb = openpyxl.load_workbook(driverSheetPath)
        self.driver_ws = self.driver_wb['Sheet1']

        # parse sheer
        self.parse()

        # debug print
        self.print()

        # create moderation sheet
        self.create()

    ##################################################################################################
    def print(self) :
        print(self.course_code, self.driver_rows, self.driver_columns)

    ##################################################################################################
    def parse(self) :
        # course code
        self.course_code = self.driver_ws["A1"].value

        self.driver_rows    = self.driver_ws.max_row
        self.driver_columns = self.driver_ws.max_column

        ####################################
        # loop over components (and sub-components)
        ####################################
        iRow  = self.iRowStart
        bMore = True
        while(bMore) :
            componentName      = self.driver_ws.cell(iRow, self.iComponentColumn).value
            componentSubName   = self.driver_ws.cell(iRow, self.iComponentColumn+1).value
            componentWeight    = self.driver_ws.cell(iRow, self.iComponentColumn+2).value
            iRow += 1

            if componentName is not None :
                self.componentName.append(componentName)
                self.componentSubName.append(componentSubName)
                self.componentWeight.append(componentWeight)

                # print(componentName,componentSubName,componentWeight,componentOutOf)

            if componentName is None :
                bMore = False

        ####################################
        # exam details
        ####################################
        self.nQuestions = self.driver_ws.cell(1,7).value
        self.nTotal     = self.driver_ws.cell(1,9).value

        iRow  = self.iRowStart
        bMore = True
        while(bMore) :
            questionName     = self.driver_ws.cell(iRow, self.iExamColumn).value
            questionWeight   = self.driver_ws.cell(iRow, self.iExamColumn+1).value
            iRow += 1

            if questionName is not None :
                self.questionName.append(questionName)
                self.questionWeight.append(questionWeight)
                # print(questionName,questionWeight)

            if questionName is None :
                bMore = False


        # loop over exam questions

    ##################################################################################################
    def create(self) :
        ms = ModerationSheet(self.driverSheetFileStub);

        # Add instructions sheet
        ms.addInstructionSheet("Instructions")

        # add driver sheet
        ms.addDriver(self.driver_ws)

        narrowest_ws = None
        componentSheets = dict()

        # loop over components
        for i, (c, cs) in enumerate(zip(self.componentName, self.componentSubName)):
            print(c,cs)
            if cs is None :
                try:
                    wb = openpyxl.load_workbook(str(self.driverSheetDir)+"/"+c+"/"+self.driverSheetFileStub+" "+c+".xlsx")
                except FileNotFoundError:
                    wb = openpyxl.load_workbook(str(self.driverSheetDir) + "/" + self.driverSheetFileStub + " " + c + ".xlsx")
            else :
                try:
                    wb = openpyxl.load_workbook(str(self.driverSheetDir)+"/"+c+"/SubComponents/"+self.driverSheetFileStub+" "+c+" "+cs+".xlsx")
                except FileNotFoundError:
                    wb = openpyxl.load_workbook(str(self.driverSheetDir) + "/" + self.driverSheetFileStub + " " + c + " " + cs + ".xlsx")
            ws = wb['Sheet1']

            if narrowest_ws is None or narrowest_ws.max_column < ws.max_column:
                narrowest_ws = ws

            if cs is None :
                componentSheets[c] = ws
                if c == "EX" or c == "EXAM":
                    ms.addExamSheet(ws,self.nQuestions, self.questionName, self.questionWeight)
                else :
                    ms.addComponentSheet(c,ws)
            else :
                ms.addSubComponentSheet(c+" "+cs,ws)

        iScaledComponentsOffset = ms.addModerationSheet(
            narrowest_ws, 3,
            self.componentName, self.componentSubName, self.componentWeight
        )

        # add scaled exam sheet
        iCom = 0
        for (c, cs) in zip(self.componentName, self.componentSubName):
            if cs is None:
                ms.addScaledSheet(
                    componentSheets[c],
                    c,
                    iScaledComponentsOffset + iCom
                )
                iCom += 1

        # add sign off
        ms.addSignOffSheet()

        # save workbook
        ms.saveWorkbook(str(self.driverSheetDir)+os.sep+self.driverSheetFileStub+" Moderation.xlsx")
