
# THIS REPO HAS MOVED

Please use the [CIM][cim] version instead..

[cim]: https://gitlab.cim.rhul.ac.uk/uxac009/epms-moderation-sheets 

# To run

GUI:

    $ python3 main.py

Command line

    $ python3 main-cmd.py <driver sheet>

# To make an EXE

Use [pyinstaller](https://pyinstaller.org/en/stable/).

Run:

    > pyinstaller.exe -F main.py

(Command line from memory, sorry if wrong.)
