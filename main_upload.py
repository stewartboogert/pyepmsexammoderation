import os
import wx
import glob
import openpyxl
import openpyxl.utils

import wx.lib.agw.multidirdialog as MDD

from openpyxl.utils import get_column_letter

import pyEPMSExams


wildcard = "Excel files (*.xlsx)|*.xlsx|" \
            "All files (*.*)|*.*"


def find_column(sheet, heading_row, title):
    for col in range(1, sheet.max_column+1):
        if sheet.cell(heading_row, col).value == title:
            return col
    raise ValueError(f"Could not find column {title} on {sheet}.")

def find_column_letter(sheet, heading_row, title):
    return get_column_letter(find_column(sheet, heading_row, title))


class MasterLookup :

    _HEADINGS_ROW = 1

    def __init__(self):
        app = wx.App(False)
        frame = wx.Frame(None, wx.ID_ANY, "Prepare Moderation Sheet for Upload")
        frame.Show()
        dlg = wx.FileDialog(frame, message="Choose a file",
                            defaultDir="",
                            defaultFile="",
                            wildcard=wildcard,
                            style=wx.FD_OPEN | wx.FD_MULTIPLE | wx.FD_CHANGE_DIR
                            )
        if dlg.ShowModal() == wx.ID_OK:
            paths = dlg.GetPaths()
            print("You chose the following file(s):")
            masterSheet = paths[0]
            print(masterSheet)

        dlg.Destroy()

        print("Loading workbook...")
        self.lookup_workbook = openpyxl.load_workbook(masterSheet)
        print("Loaded workbook")

    def find_sequenceNumberFromComponentId(self, componentId):

        componentId = int(componentId)

        try :
            return self.componentId2sequence[componentId]
        except KeyError :
            return "XXX-"+str(componentId)
        except AttributeError :
            pass

        ws = self.lookup_workbook["Components"]

        componentIdColumnLetter = find_column_letter(
            ws, self._HEADINGS_ROW, "Component ID"
        )
        sequenceNumberColumnLetter = find_column_letter(
            ws, self._HEADINGS_ROW, "Component Seq No"
        )

        self.componentId2sequence = {}

        for row in range(self._HEADINGS_ROW + 1, ws.max_row + 1) :
            crnCellName            = "{}{}".format(componentIdColumnLetter, row)
            sequenceNumberCellName = "{}{}".format(sequenceNumberColumnLetter, row)

            self.componentId2sequence[ws[crnCellName].value] = ws[sequenceNumberCellName].value

        try :
            return self.componentId2sequence[componentId]
        except KeyError :
            return "XXX-"+str(componentId)

    def find_moduleCodeFromCRN(self,crn):

        crn = str(crn)

        try :
            return self.crn2momdulecode[crn]
        except :
            pass

        ws = self.lookup_workbook["Components"]

        crnColumnLetter = find_column_letter(ws, self._HEADINGS_ROW, "CRN")
        moduleCodeColumnLetter = find_column_letter(
            ws, self._HEADINGS_ROW, "Module Code"
        )

        self.crn2modulecode = {}

        for row in range(self._HEADINGS_ROW + 1, ws.max_row + 1) :
            crnCellName            = "{}{}".format(crnColumnLetter, row)
            moduleCodeCellName     = "{}{}".format(moduleCodeColumnLetter, row)

            self.crn2modulecode[ws[crnCellName].value] = ws[moduleCodeCellName].value

        return self.crn2modulecode[crn]

    def find_studentIDFromCandidateNumber(self, candidateNumber):

        candidateNumber = str(candidateNumber)

        try :
            return self.candidate2student[candidateNumber]
        except KeyError :
            return "XXX-"+str(candidateNumber)
        except AttributeError :
            pass

        ws = self.lookup_workbook["Components"]

        candidateNumberColumnLetter = find_column_letter(
            ws, self._HEADINGS_ROW, "Candidate Number"
        )
        studentIdColumnLetter = find_column_letter(
            ws, self._HEADINGS_ROW, "Student ID"
        )

        self.candidate2student = {}

        for row in range(self._HEADINGS_ROW + 1, ws.max_row + 1) :
            candidateCellName = "{}{}".format(candidateNumberColumnLetter, row)
            studentCellName = "{}{}".format(studentIdColumnLetter, row)

            try :
                cand_no = str(ws[candidateCellName].value)
                student_no = int(ws[studentCellName].value)
                self.candidate2student[cand_no] = student_no
            except :
                print(row)
        try :
            return self.candidate2student[candidateNumber]
        except KeyError :
            return "XXX-"+str(candidateNumber)

if __name__ == "__main__":

    masterLookup = MasterLookup()

    app = wx.App(False)
    frame = wx.Frame(None, wx.ID_ANY, "Choose Input Directory")
    frame.Show()
    dlg = wx.DirDialog (None, "Choose input directory", "",
                    wx.DD_DEFAULT_STYLE | wx.DD_DIR_MUST_EXIST)

    if dlg.ShowModal() == wx.ID_OK:
        directory = dlg.GetPath()
        print("You chose the following file(s):")
        print(directory)
    dlg.Destroy()

    # get contents of directory
    moderation_workbook_paths = glob.glob(directory+"/*Moderation*.xls*")

    # open final upload sheet
    output_wb = openpyxl.Workbook()
    output_components = output_wb.create_sheet("Components")
    output_final_marks = output_wb.create_sheet("Final Marks")

    iComponentRow = 2
    iFinalScoreRow = 2

    component_studentid_column   = 1
    component_crn_column         = 2
    component_seqno_column       = 3
    component_upload_column      = 4
    component_reasoncode_column  = 5
    component_componentid_column = 6
    module_code_column           = 7
    component_name_column        = 8

    output_components.cell(1,component_studentid_column).value  = "ID number"
    output_components.cell(1,component_crn_column).value        = "CRN"
    output_components.cell(1,component_seqno_column).value      = "Seq No."
    output_components.cell(1,component_upload_column).value     = "For IT upload"
    output_components.cell(1,component_reasoncode_column).value = "Reason Code"
    output_components.cell(1,module_code_column).value          = "Module Code"
    output_components.cell(1,component_name_column).value       = "Component Name"

    final_studentid_column   = 1
    final_crn_column         = 2
    final_module_code_column = 3
    final_mark_column        = 4

    output_final_marks.cell(1,final_studentid_column).value   = "ID number"
    output_final_marks.cell(1,final_crn_column).value         = "CRN"
    output_final_marks.cell(1,final_module_code_column).value = "Module Code"
    output_final_marks.cell(1,final_mark_column).value        = "Final Mark"

    # loop over files
    for moderation_workbook_path in moderation_workbook_paths :
        print("------------------------")
        print(moderation_workbook_path)

        # skip temporary files
        basename = os.path.basename(moderation_workbook_path)
        if basename.find("~") != -1 :
            continue

        if basename.find("upload") != -1 :
            continue

        # open moderation workbook
        moderation_workbook = openpyxl.load_workbook(
            moderation_workbook_path, data_only=True
        )

        # component sheet
        moderation_sheet_banner = moderation_workbook["Banner"]

        moduleCode = moderation_sheet_banner.cell(1, 1).value

        iRowStart           = 3
        iComponentColumn    = 1

        bMore = True

        while(bMore) :
            componentName      = moderation_sheet_banner.cell(iRowStart, iComponentColumn).value

            print(componentName)

            iRowStart += 1

            if componentName is None :
                bMore = False
            else:
                if componentName != "EX" :
                    moderation_sheet_component = moderation_workbook[componentName]
                else :
                    moderation_sheet_component = moderation_workbook["EX Mark for Upload (scaled)"]

                print(componentName, moderation_sheet_component.max_row+1)

                cand_no_col = find_column(
                    moderation_sheet_component, 1, "Candidate Number"
                )
                crn_col = find_column(
                    moderation_sheet_component, 1, "CRN"
                )
                score_col = find_column(
                    moderation_sheet_component, 1, "Score"
                )
                try:
                    reason_code_col = find_column(
                        moderation_sheet_component, 1, "Grade Change Reason"
                    )
                except ValueError:
                    # fix for a bug in old moderation sheet creator
                    reason_code_col = find_column(
                        moderation_sheet_component, 1, "Notes"
                    )

                for iRow in range(2,moderation_sheet_component.max_row+1):
                    try:
                        cand_no = moderation_sheet_component.cell(
                            iRow, cand_no_col
                        ).value
                        student_id \
                            = masterLookup.find_studentIDFromCandidateNumber(
                                cand_no
                            )
                        crn = moderation_sheet_component.cell(
                            iRow,crn_col
                        ).value
                        seqNo = masterLookup.find_sequenceNumberFromComponentId(moderation_sheet_component.cell(iRow,3).value)
                        raw_score = moderation_sheet_component.cell(
                            iRow, score_col
                        ).value
                        try :
                            upload_value = round(float(raw_score),0)
                        except :
                            upload_value = raw_score

                        reason_code = moderation_sheet_component.cell(
                            iRow, reason_code_col
                        ).value

                        output_components.cell(iComponentRow, component_studentid_column).value = student_id
                        output_components.cell(iComponentRow, component_crn_column).value = crn
                        output_components.cell(iComponentRow, component_seqno_column).value = seqNo
                        output_components.cell(iComponentRow, component_upload_column).value  = upload_value
                        output_components.cell(iComponentRow, component_reasoncode_column).value = reason_code
                        output_components.cell(iComponentRow, module_code_column).value = moduleCode
                        output_components.cell(iComponentRow, component_name_column).value = componentName

                        iComponentRow += 1
                    except Exception as e:
                        print(f"Ignoring row {iRow} with error: ", e)

        # After components, fill in final marks
        # This really needs breaking into functions, sorry
        moderation_sheet = moderation_workbook["Moderation"]
        print("Final marks", moderation_sheet.max_row+1)

        cand_no_col = find_column(moderation_sheet, 1, "Candidate Number")
        crn_col = find_column(moderation_sheet, 1, "CRN")
        score_col = find_column(moderation_sheet, 1, "Total (scaled)")

        for iRow in range(2,moderation_sheet_component.max_row+1):
            try:
                cand_no = moderation_sheet.cell(iRow, cand_no_col).value
                student_id \
                    = masterLookup.find_studentIDFromCandidateNumber(
                        cand_no
                    )
                crn = moderation_sheet.cell(iRow,crn_col).value
                final_score = moderation_sheet.cell(
                    iRow, score_col
                ).value
                try :
                    upload_value = round(float(final_score),0)
                except :
                    upload_value = final_score

                output_final_marks.cell(
                    iFinalScoreRow, final_studentid_column
                ).value = student_id
                output_final_marks.cell(
                    iFinalScoreRow, final_crn_column
                ).value = crn
                output_final_marks.cell(
                    iFinalScoreRow, final_module_code_column
                ).value = moduleCode
                output_final_marks.cell(
                    iFinalScoreRow, final_mark_column
                ).value = final_score

                iFinalScoreRow += 1
            except Exception as e:
                print(f"Ignoring row {iRow} with error: ", e)

    output_wb.save(directory+"/upload.xlsx")

    # app.MainLoop()
