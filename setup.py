from setuptools import setup, find_packages

setup(
    name='pyEPMSExams',
    version='0.2',
    packages=find_packages(exclude=["docs"]),
    # Not sure how strict these need to be...
    install_requires=["openpyxl",
                      "wx"],
    setup_requires=[],
    tests_require=[],
    python_requires=">=3.7.*",
    author='Stewart Boogert',
    author_email='stewart.boogerts@rhul.ac.uk',
    description=("Python utilities for EPMS exams"),
    license="GPL3",
)
