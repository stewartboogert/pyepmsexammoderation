import os
import wx
import wx.lib.agw.multidirdialog as MDD

import pyEPMSExams

wildcard = "Excel files (*.xlsx)|*.xlsx|" \
            "All files (*.*)|*.*"

if __name__ == "__main__":
    try:
        app = wx.App(False)
        frame = wx.Frame(None, wx.ID_ANY, "Module Sheet Creator")
        dlg = wx.FileDialog(frame, message="Choose a file",
                            defaultDir="",
                            defaultFile="",
                            wildcard=wildcard,
                            style=wx.FD_OPEN | wx.FD_MULTIPLE | wx.FD_CHANGE_DIR
                            )
        if dlg.ShowModal() == wx.ID_OK:
            paths = dlg.GetPaths()
            print("You chose the following file(s):")
            driverSheet = paths[0]
            print(driverSheet)
            c = pyEPMSExams.ModerationSheetCreator(driverSheet)
        dlg.Close()
        frame.Close()

    except FileNotFoundError as e:
        print(e)
    except Exception as e:
        print(e)
    finally:
        input("All done, please press enter or close the window.")
